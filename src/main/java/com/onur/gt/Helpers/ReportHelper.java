package com.onur.gt.Helpers;

import java.util.HashMap;
import java.util.regex.Pattern;

public interface ReportHelper {

    // mevcut txt dosyalarından verileri okur ve hashmap'e atar
    HashMap<String, String> putValuesToHashmap(String file_name);
    // raporu generate eder
    void createReport(int report_type, String dataFilePath);

}
