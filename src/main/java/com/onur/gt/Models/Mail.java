package com.onur.gt.Models;

import lombok.Getter;

public class Mail {

    @Getter
    private long musteri_no;
    @Getter
    private char adres_tipi;
    @Getter
    private String adres;

    public Mail(long musteri_no,char adres_tipi, String adres){
        this.musteri_no = musteri_no;
        this.adres_tipi = adres_tipi;
        this.adres = adres;
    }

    @Override
    public String toString() {
        return "Mail{" +
                "musteri_no=" + musteri_no +
                ", adres_tipi=" + adres_tipi +
                ", adres='" + adres + '\'' +
                '}';
    }
}
