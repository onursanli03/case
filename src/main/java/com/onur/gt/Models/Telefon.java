package com.onur.gt.Models;

import lombok.Getter;

public class Telefon {

    @Getter
    private long musteri_no;
    @Getter
    private char telefon_tipi;
    @Getter
    private int ulke_kodu;
    @Getter
    private int alan_kodu;
    @Getter
    private long numara;

    public Telefon(long musteri_no, char telefon_tipi, int ulke_kodu, int alan_kodu, long numara) {
        this.musteri_no = musteri_no;
        this.telefon_tipi = telefon_tipi;
        this.ulke_kodu = ulke_kodu;
        this.alan_kodu = alan_kodu;
        this.numara = numara;
    }

    @Override
    public String toString() {
        return "Telefon{" +
                "musteri_no=" + musteri_no +
                ", telefon_tipi=" + telefon_tipi +
                ", ulke_kodu=" + ulke_kodu +
                ", alan_kodu=" + alan_kodu +
                ", numara=" + numara +
                '}';
    }
}
