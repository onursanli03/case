package com.onur.gt.Helpers;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class ReportCreator implements ReportHelper{

    private final String RAPOR_PATH="Files/Reports";
    private LocalDate reportDate; // raporun oluşturulduğu tarih
    private long reportVersionNum = 000; // raporun versiyon numarası

    // girilen inputa göre aynı rapordan olup olmadığını kontrol eder
    public boolean IsReportExits(String path) {
        boolean exits;
        File tempFile = new File(path);
        exits = tempFile.exists();
        return exits;
    }


    @Override
    public HashMap<String, String> putValuesToHashmap(String file_name) {

        HashMap<String,String> resultHashMap = new HashMap<>();

        String line;
        String [] tmpStrDatas;

        try{
            FileReader fileReader = new FileReader(file_name);

            BufferedReader bufferedReader = new BufferedReader(fileReader);

            bufferedReader.readLine();
            while((line = bufferedReader.readLine())!= null){

                tmpStrDatas = line.split(";",4);
                resultHashMap.put(tmpStrDatas[0],tmpStrDatas[1]);
                resultHashMap.put(tmpStrDatas[0],tmpStrDatas[2]);
                resultHashMap.put(tmpStrDatas[0],tmpStrDatas[3]);
            }
            bufferedReader.close();
        }catch (FileNotFoundException ex){
            System.out.println(
                    "Dosya açılamadı '" +
                            file_name + "'");
        } catch (IOException ex) {
            System.out.println(
                    "Dosyayı okurken bir hata oluştu '"
                            + file_name + "'");
        }
        return resultHashMap;
    }

    @Override
    public void createReport(int report_type, String dataFilePath) {

        switch (report_type){
            case 10:
                try{
                    int size;
                    size = putValuesToHashmap(dataFilePath).size();
                    reportDate = LocalDate.now();
                    String path = RAPOR_PATH +"/RAPOR_"+report_type+"_"+reportDate.toString()+"_V_"+reportVersionNum+".txt";
                    if(IsReportExits(path)){
                        reportVersionNum++;
                        path = RAPOR_PATH +"/RAPOR_"+report_type+"_"+reportDate.toString()+"_V_"+reportVersionNum+".txt";
                        FileWriter fw = new FileWriter(path);
                        fw.write(size);
                        fw.close();
                    }else{
                        FileWriter fw = new FileWriter(path);
                        fw.write(size);
                        fw.close();
                    }
                }catch (IOException e){
                    System.out.println("Rapor yazılamadı" + e);
                }
                break;
            case 11:
                try{
                    int tCount;
                    int gCount;
                    tCount= Collections.frequency(putValuesToHashmap(dataFilePath).values(),"T");
                    gCount = Collections.frequency(putValuesToHashmap(dataFilePath).values(),"G");
                    reportDate = LocalDate.now();
                    String path = RAPOR_PATH +"/RAPOR_"+report_type+"_"+reportDate.toString()+"_V_"+reportVersionNum+".txt";
                    if (IsReportExits(path)){
                        reportVersionNum++;
                        path = RAPOR_PATH +"/RAPOR_"+report_type+"_"+reportDate.toString()+"_V_"+reportVersionNum+".txt";
                        FileWriter fw = new FileWriter(path);
                        fw.write("T"+tCount);
                        fw.write("G"+gCount);
                        fw.close();
                    }else{
                        FileWriter fw = new FileWriter(path);
                        fw.write("T"+tCount);
                        fw.write("G"+gCount);
                        fw.close();
                    }
                }catch (IOException e){
                    System.out.println("Rapor yazılamadı" + e);
                }
                break;
            case 20:
                try{
                    int size;
                    size = putValuesToHashmap(dataFilePath).size();
                    reportDate = LocalDate.now();
                    String path = RAPOR_PATH +"/RAPOR_"+report_type+"_"+reportDate.toString()+"_V_"+reportVersionNum+".txt";
                    if(IsReportExits(path)){
                        reportVersionNum++;
                        path = RAPOR_PATH +"/RAPOR_"+report_type+"_"+reportDate.toString()+"_V_"+reportVersionNum+".txt";
                        FileWriter fw = new FileWriter(path);
                        fw.write(size);
                        fw.close();
                    }else{
                        FileWriter fw = new FileWriter(path);
                        fw.write(size);
                        fw.close();
                    }
                }catch (IOException e){
                    System.out.println("Rapor yazılamadı" + e);
                }
                break;
            case 21:
                try{
                    int tCount = Collections.frequency(putValuesToHashmap(dataFilePath).values(),"T");
                    int fCount = Collections.frequency(putValuesToHashmap(dataFilePath).values(),"F");
                    int cCount = Collections.frequency(putValuesToHashmap(dataFilePath).values(),"C");
                    reportDate = LocalDate.now();
                    String path = RAPOR_PATH +"/RAPOR_"+report_type+"_"+reportDate.toString()+"_V_"+reportVersionNum+".txt";
                    if (IsReportExits(path)){
                        reportVersionNum++;
                        path = RAPOR_PATH +"/RAPOR_"+report_type+"_"+reportDate.toString()+"_V_"+reportVersionNum+".txt";
                        FileWriter fw = new FileWriter(path);
                        fw.write("T"+tCount);
                        fw.write("F"+fCount);
                        fw.write("C"+cCount);
                        fw.close();
                    }else{
                        FileWriter fw = new FileWriter(path);
                        fw.write("T"+tCount);
                        fw.write("F"+fCount);
                        fw.write("C"+cCount);
                        fw.close();
                    }
                }catch (IOException e){
                    System.out.println("Rapor yazılamadı" + e);
                }
                break;
            case 30:
                try{
                    int size;
                    size = putValuesToHashmap(dataFilePath).size();
                    reportDate = LocalDate.now();
                    String path = RAPOR_PATH +"/RAPOR_"+report_type+"_"+reportDate.toString()+"_V_"+reportVersionNum+".txt";
                    if(IsReportExits(path)){
                        reportVersionNum++;
                        path = RAPOR_PATH +"/RAPOR_"+report_type+"_"+reportDate.toString()+"_V_"+reportVersionNum+".txt";
                        FileWriter fw = new FileWriter(path);
                        fw.write(size);
                        fw.close();
                    }else{
                        FileWriter fw = new FileWriter(path);
                        fw.write(size);
                        fw.close();
                    }
                }catch (IOException e){
                    System.out.println("Rapor yazılamadı" + e);
                }
                break;
            case 31:
                try{
                    int wCount;
                    int eCount;
                    wCount= Collections.frequency(putValuesToHashmap(dataFilePath).values(),"W");
                    eCount = Collections.frequency(putValuesToHashmap(dataFilePath).values(),"E");
                    reportDate = LocalDate.now();
                    String path = RAPOR_PATH +"/RAPOR_"+report_type+"_"+reportDate.toString()+"_V_"+reportVersionNum+".txt";
                    if (IsReportExits(path)){
                        reportVersionNum++;
                        path = RAPOR_PATH +"/RAPOR_"+report_type+"_"+reportDate.toString()+"_V_"+reportVersionNum+".txt";
                        FileWriter fw = new FileWriter(path);
                        fw.write("W"+wCount);
                        fw.write("E"+eCount);
                        fw.close();
                    }else{
                        FileWriter fw = new FileWriter(path);
                        fw.write("W"+wCount);
                        fw.write("E"+eCount);
                        fw.close();
                    }
                }catch (IOException e){
                    System.out.println("Rapor yazılamadı" + e);
                }
                break;

        }



    }


}
