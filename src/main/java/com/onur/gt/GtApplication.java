package com.onur.gt;

import com.onur.gt.Helpers.ReportCreator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
public class GtApplication {

    public static void main(String[] args) {
        SpringApplication.run(GtApplication.class, args);
        ReportCreator creator = new ReportCreator();
        int report_type;
        String path;
        int [] report_types = {10,11,12,13,14,20,21,22,30,31};
        Scanner sc = new Scanner(System.in);
        System.out.println("Dosyaların bulunduğu path'i giriniz!:");
        path = sc.nextLine();
        while(sc.hasNext()){
            System.out.println("Raport tipini giriniz! :");
            report_type = sc.nextInt();
            for (int i = 0; i < report_types.length;i++){
                if (report_type != report_types[i]){
                    System.out.println("Hatalı giriş yaptınız tekrar deneyiniz");
                    report_type = sc.nextInt();
                    creator.createReport(report_type,path);
                }else{
                    creator.createReport(report_type,path);
                }
            }
        }
    }

}
