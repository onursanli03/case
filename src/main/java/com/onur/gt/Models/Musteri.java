package com.onur.gt.Models;

import lombok.Getter;

public class Musteri {

    @Getter
    private long musteri_no;
    @Getter
    private char musteri_tipi;
    @Getter
    private String musteri_adi;
    @Getter
    private String musteri_soyadi;

    public Musteri(long musteri_no, char musteri_tipi, String musteri_adi, String musteri_soyadi){
        this.musteri_no = musteri_no;
        this.musteri_tipi = musteri_tipi;
        this.musteri_adi = musteri_adi;
        this.musteri_soyadi = musteri_soyadi;
    }

    @Override
    public String toString() {
        return "Musteri{" +
                "musteri_no=" + musteri_no +
                ", musteri_tipi=" + musteri_tipi +
                ", musteri_adi='" + musteri_adi + '\'' +
                ", musteri_soyadi='" + musteri_soyadi + '\'' +
                '}';
    }
}
